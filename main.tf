## /etc/systemd/system/kafka.service
[Unit]
Description=Kafka Service
After=network.target
RequiresMountsFor=/var/log/kafka

[Service]
Type=simple
User=root
ExecStart=/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
Restart=on-abort


[Install]